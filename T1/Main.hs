
-- Modulo principal do trabalho

module Main (main) where

import Matriz

import BackTrack

import Teste

paraVetor :: [[Seta]] -> [Seta]
paraVetor (a:b) = a
paraVetor [] = []

main = do
	putStrLn(formataMatriz(trasformaMatrizString matriz))
	
--1x1
	--let resposta = (procuraSolucao1x1)

--2x2
	--let resposta = (procuraSolucao2x2)

--3x3
	--let resposta = (procuraSolucao3x3)

--4x4
	--let resposta = (procuraSolucao4x4)

--5x5
	let resposta = (procuraSolucao5x5)

--Mostra solucao
	print(map showS(paraVetor resposta))


	