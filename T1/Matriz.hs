
-- Modulo com os metodos para trabalhar com a matriz (tabuleiro)

module Matriz (testaSetas, 
				Seta(Norte,Nordeste,Leste,Sudeste,Sul,Sudoeste,Oeste,Noroeste),
				formataMatriz,
				stringLinha,
				trasformaMatrizString,
				showS, testeSetas
			) where

import Teste

-- Matriz para teste do trabalho
-- Alterar a vontade (soh deixa comentado para termos mais testes)
matrizTeste :: [[Int]]
matrizTeste = matriz

tamanhoVetor :: Int
tamanhoVetor = vetor

-- Criando um data Seta com todos os possiveis tipos
data Seta = Norte |
			Nordeste |
			Leste |
			Sudeste |
			Sul |
			Sudoeste |
			Oeste |
			Noroeste
			deriving (Show)

-- Diminui um elemento da matriz para fazer o teste das setas
-- @param [[Int]] = matriz antiga
-- @param Int = linha do elemento
-- @param Int = coluna do elemento
-- @return [[Int]] = matriz nova
diminuirElementoMatriz :: [[Int]] -> Int -> Int -> [[Int]]
diminuirElementoMatriz [] _ _ = []
diminuirElementoMatriz (a:b) 0 coluna = [(diminuirElementoVetor a coluna)] ++ b
diminuirElementoMatriz (a:b) linha coluna = a : (diminuirElementoMatriz b (linha-1) coluna)

-- Diminui elemento em uma linha da matriz
-- @param [Int] = linha a qual eh desejado diminuir valor
-- @param Int = coluna desejada
-- @return [Int] = nova linha
diminuirElementoVetor :: [Int] -> Int -> [Int]
diminuirElementoVetor [] _ = []
diminuirElementoVetor (a:b) 0 = [(a-1)] ++ b
diminuirElementoVetor (a:b) coluna = [a] ++ (diminuirElementoVetor b (coluna-1))						

-- Testa um conjunto de setas e executa a estrategia
-- @param [[Int]] = matriz usada no teste
-- @param [Seta] = conjunto de setas a serem testadas
-- @param Int = posicao atual do vetor de setas
-- @return [[Int]] = matriz resultante do teste
testeSetas :: [[Int]] -> [Seta] -> Int -> [[Int]]
testeSetas m [] _ = m
testeSetas m (Sul:b) n = 
	if (n < (nLinhas m))
		then (testeSetas (diminuirEmColuna m (nLinhas m) n) b (n+1))
		else (testeSetas m b (n+1))
testeSetas m (Oeste:b) n = 
	if ((n > ((tamanhoVetor `div` 4)-1)) && (n < (tamanhoVetor `div` 2)) )
		then (testeSetas (diminuirEmLinha m (n `mod` (nLinhas m)) (nColunas m)) b (n+1))
		else (testeSetas m b (n+1))
testeSetas m (Norte:b) n = 
	if ( (n > ((tamanhoVetor `div` 2)-1)) && (n < (tamanhoVetor - (nLinhas m))))
		then (testeSetas (diminuirEmColuna m (nLinhas m) (((nLinhas m)-1)-(n `mod` (nLinhas m)))) b (n+1))
		else (testeSetas m b (n+1))
testeSetas m (Leste:b) n = 
	if (n > ((tamanhoVetor - (nLinhas m))-1))
		then (testeSetas (diminuirEmLinha m (((nLinhas m)-1)-(n `mod` (nLinhas m))) (nLinhas m)) b (n+1))
		else (testeSetas m b (n+1))
testeSetas m (Sudeste:b) n = 
	if (n < (nLinhas m))
		then (testeSetas (diminuirEmdiagonalSE m 0 (n+1) (nLinhas m)) b (n+1))
		else if (n > ((tamanhoVetor - (nLinhas m))-1))
			then (testeSetas (diminuirEmdiagonalSE m (((nLinhas m)-1)-(n `mod` (nLinhas m))+1) 0 (nLinhas m)) b (n+1))
			else (testeSetas m b (n+1))
testeSetas m (Sudoeste:b) n = 
	if (n < (nLinhas m))
		then (testeSetas (diminuirEmdiagonalSW m 0 (n-1) (nLinhas m)) b (n+1))
		else if ((n > ((tamanhoVetor `div` 4)-1)) && (n < (tamanhoVetor `div` 2)) )
			then (testeSetas (diminuirEmdiagonalSW m ((n `mod` (nLinhas m))+1) ((nLinhas m)-1) (nLinhas m)) b (n+1))
			else (testeSetas m b (n+1))
testeSetas m (Nordeste:b) n = 
	if ( (n > ((tamanhoVetor `div` 2)-1)) && (n < (tamanhoVetor - (nLinhas m))))
		then (testeSetas (diminuirEmdiagonalNE m ((nLinhas m)-1) (((nLinhas m)-1)-(n `mod` (nLinhas m))+1) (nLinhas m)) b (n+1))
		else if (n > ((tamanhoVetor - (nLinhas m))-1))
			then (testeSetas (diminuirEmdiagonalNE m (((nLinhas m)-1)-(n `mod` (nLinhas m))-1) 0 (nLinhas m)) b (n+1))
			else (testeSetas m b (n+1))
testeSetas m (Noroeste:b) n = 
	if ((n > ((tamanhoVetor `div` 4)-1)) && (n < (tamanhoVetor `div` 2)) )
		then (testeSetas (diminuirEmdiagonalNW m ((n `mod` (nLinhas m))-1) ((nLinhas m)-1) (nLinhas m)) b (n+1))
		else if ( (n > ((tamanhoVetor `div` 2)-1)) && (n < (tamanhoVetor - (nLinhas m))))
			then (testeSetas (diminuirEmdiagonalNW m ((nLinhas m)-1) (((nLinhas m)-1)-(n `mod` (nLinhas m))-1) (nLinhas m)) b (n+1))
			else (testeSetas m b (n+1))

-- Testa se a matriz resultante eh zerada com o teste das setas
testaSetas :: [[Int]] -> [Seta] -> Bool
testaSetas m v = (matrizZerada ((testeSetas m v 0)))

-- Verifica se a matriz soh possui elementos iguais a zero
matrizZerada :: [[Int]] -> Bool
matrizZerada [] = True
matrizZerada (a:b) = (vetorZerado a) && (matrizZerada b)

--Verifica se um vetor soh possui elementos iguais a zero
vetorZerado :: [Int] -> Bool
vetorZerado [] = True
vetorZerado (a:b) = 
	if (a == 0)
		then True && (vetorZerado b)
		else False

-- Decrementa os elementos de uma matriz em uma certa coluna
-- @param [[Int]] = matriz a decrementar
-- @param Int = linha atual
-- @param Int = coluna a modificar (fixo)
-- @return [[Int]] = matriz modificada
diminuirEmColuna :: [[Int]] -> Int -> Int -> [[Int]]
diminuirEmColuna m 0 coluna = (diminuirElementoMatriz m 0 coluna)
diminuirEmColuna m linha coluna = (diminuirEmColuna 
									(diminuirElementoMatriz m linha coluna) (linha-1) coluna
								  )

-- Decrementa os elementos de uma matriz em uma certa linha
-- @param [[Int]] = matriz a decrementar
-- @param Int = linha a modificar (fixa)
-- @param Int = coluna a atual
-- @return [[Int]] = matriz modificada
diminuirEmLinha :: [[Int]] -> Int -> Int -> [[Int]]
diminuirEmLinha m linha 0 = (diminuirElementoMatriz m linha 0)
diminuirEmLinha m linha coluna = (diminuirEmLinha 
									(diminuirElementoMatriz m linha coluna) (linha) (coluna-1)
								  )


-- Decrementa os elementos de uma matriz em SE apartir de uma posicao
-- @param [[Int]] = matriz a decrementar
-- @param Int = linha 
-- @param Int = coluna
-- @param Int = tamanho da matriz
-- @return [[Int]] = matriz modificada
diminuirEmdiagonalSE::[[Int]] -> Int -> Int -> Int -> [[Int]]
diminuirEmdiagonalSE m linha coluna tam =
	if((linha == tam -1) || (coluna == tam -1))
		then (diminuirElementoMatriz m linha coluna)
		else (diminuirEmdiagonalSE (diminuirElementoMatriz m linha coluna) (linha + 1) (coluna + 1) tam)

-- Decrementa os elementos de uma matriz em SW apartir de uma posicao
-- @param [[Int]] = matriz a decrementar
-- @param Int = linha 
-- @param Int = coluna
-- @param Int = tamanho da matriz
-- @return [[Int]] = matriz modificada
diminuirEmdiagonalSW::[[Int]] -> Int -> Int -> Int -> [[Int]]
diminuirEmdiagonalSW m linha coluna tam =
	if((linha == tam -1) || (coluna == 0))
		then (diminuirElementoMatriz m linha coluna)
		else (diminuirEmdiagonalSW (diminuirElementoMatriz m linha coluna) (linha + 1) (coluna - 1) tam)

-- Decrementa os elementos de uma matriz em NE apartir de uma posicao
-- @param [[Int]] = matriz a decrementar
-- @param Int = linha 
-- @param Int = coluna
-- @param Int = tamanho da matriz
-- @return [[Int]] = matriz modificada
diminuirEmdiagonalNE::[[Int]] -> Int -> Int -> Int -> [[Int]]
diminuirEmdiagonalNE m linha coluna tam =
	if((linha == 0) || (coluna == tam-1))
		then (diminuirElementoMatriz m linha coluna)
		else (diminuirEmdiagonalNE (diminuirElementoMatriz m linha coluna) (linha - 1) (coluna + 1) tam)

-- Decrementa os elementos de uma matriz em NW apartir de uma posicao
-- @param [[Int]] = matriz a decrementar
-- @param Int = linha 
-- @param Int = coluna
-- @param Int = tamanho da matriz
-- @return [[Int]] = matriz modificada
diminuirEmdiagonalNW::[[Int]] -> Int -> Int -> Int -> [[Int]]
diminuirEmdiagonalNW m linha coluna tam =
	if((linha == 0) || (coluna == 0))
		then (diminuirElementoMatriz m linha coluna)
		else (diminuirEmdiagonalNW (diminuirElementoMatriz m linha coluna) (linha - 1) (coluna - 1) tam)

-- Calcula numero de linhas da matriz
-- @param [[Int]] = matriz a ser calculado o nr de linhas
-- @return = numero de linhas da matriz
nLinhas :: [[Int]] -> Int
nLinhas [] = 0
nLinhas (l:c) = 1 + (nLinhas c)

-- Calcula numero de colunas da matriz
-- @param [[Int]] = matriz a ser calculado o nr de colunas
-- @return = numero de colunas da matriz
nColunas :: [[Int]] -> Int
nColunas (l:c) = (nElementos l)

-- Calcula numero de elementos da matriz
-- @param [[Int]] = matriz a ser calculado o nr de elementos
-- @return = numero de elementos da matriz
nElementos :: [Int] -> Int
nElementos [] = 0
nElementos (a:b) = 1 + (nElementos b)

-- Trasforma uma matriz de String em uma String formadada para ser mostrada na tela
-- OBS: usar putStr ou putStrLn 
-- @param [[String]] = matriz a ser trasformada
formataMatriz:: [[String]] -> String
formataMatriz [] = ""
formataMatriz (a:b)= ((stringLinha a) ++ "\n") ++ (formataMatriz b)

-- Trasforma um vetor de String em uma String formatada
-- @param [String] = vetor a ser trasformado
stringLinha:: [String] -> String
stringLinha []= ""
stringLinha (a:b)= a ++ " " ++ stringLinha(b)


-- Trasforma uma matriz de Int em uma matriz de String
-- @param [[Int]] = matriz a ser trasformada
trasformaMatrizString::[[Int]]->[[String]]
trasformaMatrizString [] = []
trasformaMatrizString (a:b) = [(map show a)] ++ trasformaMatrizString b

-- Trasforma uma Seta em uma String abreviada
-- @param Seta = uma seta qualquer
showS::Seta->String
showS Norte = "N"
showS Nordeste = "NE"
showS Leste = "E"
showS Sudeste = "SE"
showS Sul = "S"
showS Sudoeste = "SW"
showS Oeste = "W"
showS Noroeste = "NW"