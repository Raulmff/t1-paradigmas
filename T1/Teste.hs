
-- Modulo que controla os testes do trabalho

module Teste (matriz, vetor) where

-- Matriz para teste do trabalho
matriz :: [[Int]]
--1x1
--matriz = [[4]]

--2x2 
--matriz = [[3,5],[3,4]]
--matriz = [[4,4],[4,4]]
--matriz = [[0,0],[0,0]]
--matriz = [[0,3],[3,6]]


--3x3 
--matriz = [[5,2,5],[3,0,1],[4,3,4]]
--matriz = [[2,1,7],[5,9,1],[1,1,1]]

--4x4 
--matriz = [[4,3,3,0],[7,3,3,2],[5,3,3,2],[3,1,3,0]]
--matriz = [[4,4,4,4],[4,4,4,4],[4,4,4,4],[4,4,4,4]]
--matriz = [[6,2,2,6],[2,0,0,2],[2,0,0,2],[6,2,2,6]]

--5x5
matriz =[[2,2,2,2,2],[2,0,6,0,2],[2,6,4,6,2],[2,0,6,0,2],[2,2,2,2,2]]


-- Retorna a quantidade de setas que tera para determinado tamanho de tabuleiro
--1x1 = 4
--2x2 = 8
--3x3 = 12
--4x4 = 16
--5x5 = 20
vetor :: Int
--vetor = 4
--vetor = 8
--vetor = 12
--vetor = 16
vetor = 20
