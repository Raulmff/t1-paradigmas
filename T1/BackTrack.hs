
-- Modulo com os metodos para encontrar a solucao utilizando Monads

-- Backtrack baseado em <http://www.randomhacks.net/2007/03/12/monads-in-15-minutes/>

module BackTrack (procuraSolucao1x1,procuraSolucao2x2,procuraSolucao3x3,procuraSolucao4x4,procuraSolucao5x5 ) where

import Teste

import Matriz

-- Matriz para teste do trabalho
-- Recebe o valor do modulo Teste (matriz)
matrizTeste :: [[Int]]
matrizTeste = matriz

type Choice a = [a]

choose :: [a] -> Choice a
choose xs = xs

mzero :: Choice a
mzero = choose []

guard :: Bool -> Choice ()
guard True  = return ()
guard False = mzero

procuraSolucao1x1 ::[[Seta]]
procuraSolucao1x1 = do
	x <- choose [Sul]
	y <- choose [Oeste]
	z <- choose [Norte]
	w <- choose [Leste]
	guard (testaSetas matrizTeste [x,y,z,w])
	return [x,y,z,w]

procuraSolucao2x2 :: [[Seta]]
procuraSolucao2x2 = do
	a <- choose [Sul,Sudeste]
	b <- choose [Sul,Sudoeste]
	c <- choose [Oeste,Sudoeste]
	d <- choose [Oeste,Noroeste]
	e <- choose [Norte,Noroeste]
	f <- choose [Norte,Nordeste]
	g <- choose [Leste,Nordeste]
	h <- choose [Leste,Sudeste]
	guard (testaSetas matrizTeste [a,b,c,d,e,f,g,h])
	return [a,b,c,d,e,f,g,h]

procuraSolucao3x3 :: [[Seta]]
procuraSolucao3x3 = do
	a <- choose [Sul,Sudeste]
	b <- choose [Sul,Sudeste,Sudoeste]
	c <- choose [Sul,Sudoeste]
	d <- choose [Oeste,Sudoeste]
	e <- choose [Oeste,Noroeste,Sudoeste]
	f <- choose [Oeste,Noroeste]
	g <- choose [Norte,Noroeste]
	h <- choose [Norte,Noroeste,Nordeste]
	i <- choose [Norte,Nordeste]
	j <- choose [Leste,Nordeste]
	k <- choose [Leste,Nordeste,Sudeste]
	l <- choose [Leste,Sudeste]
	guard (testaSetas matrizTeste [a,b,c,d,e,f,g,h,i,j,k,l])
	return [a,b,c,d,e,f,g,h,i,j,k,l]

procuraSolucao4x4 ::[[Seta]]
procuraSolucao4x4 = do
	a <- choose [Sul,Sudeste]
	b <- choose [Sul,Sudeste,Sudoeste]
	c <- choose [Sul,Sudeste,Sudoeste]
	d <- choose [Sul,Sudoeste]
	e <- choose [Oeste,Sudoeste]
	f <- choose [Oeste,Noroeste,Sudoeste]
	g <- choose [Oeste,Noroeste,Sudoeste]
	h <- choose [Oeste,Noroeste]
	i <- choose [Norte,Noroeste]
	j <- choose [Norte,Noroeste,Nordeste]
	k <- choose [Norte,Noroeste,Nordeste]
	l <- choose [Norte,Nordeste]
	m <- choose [Leste,Nordeste]
	n <- choose [Leste,Nordeste,Sudeste]
	o <- choose [Leste,Nordeste,Sudeste]
	p <- choose [Leste,Sudeste]
	guard (testaSetas matrizTeste [a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p])
	return [a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p]

procuraSolucao5x5 ::[[Seta]]
procuraSolucao5x5 = do
	a <- choose [Sul,Sudeste]
	b <- choose [Sul,Sudeste,Sudoeste]
	c <- choose [Sul,Sudeste,Sudoeste]
	d <- choose [Sul,Sudeste,Sudoeste]
	e <- choose [Sul,Sudoeste]
	f <- choose [Oeste,Sudoeste]
	g <- choose [Oeste,Noroeste,Sudoeste]
	h <- choose [Oeste,Noroeste,Sudoeste]
	i <- choose [Oeste,Noroeste,Sudoeste]
	j <- choose [Oeste,Noroeste]
	k <- choose [Norte,Noroeste]
	l <- choose [Norte,Noroeste,Nordeste]
	m <- choose [Norte,Noroeste,Nordeste]
	n <- choose [Norte,Noroeste,Nordeste]
	o <- choose [Norte,Nordeste]
	p <- choose [Leste,Nordeste]
	q <- choose [Leste,Nordeste,Sudeste]
	r <- choose [Leste,Nordeste,Sudeste]
	s <- choose [Leste,Nordeste,Sudeste]
	t <- choose [Leste,Sudeste]
	guard (testaSetas matrizTeste [a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t])
	return [a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t]
